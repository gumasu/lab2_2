package th.ac.tu.siit.calculator;



import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
	implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ((Button)findViewById(R.id.num0)).setOnClickListener(this);
		((Button)findViewById(R.id.num1)).setOnClickListener(this);
		((Button)findViewById(R.id.num2)).setOnClickListener(this);
		((Button)findViewById(R.id.num3)).setOnClickListener(this);
		((Button)findViewById(R.id.num4)).setOnClickListener(this);
		((Button)findViewById(R.id.num5)).setOnClickListener(this);
		((Button)findViewById(R.id.num6)).setOnClickListener(this);
		((Button)findViewById(R.id.num7)).setOnClickListener(this);
		((Button)findViewById(R.id.num8)).setOnClickListener(this);
		((Button)findViewById(R.id.num9)).setOnClickListener(this);
		
		((Button)findViewById(R.id.add)).setOnClickListener(this);
		((Button)findViewById(R.id.sub)).setOnClickListener(this);
		((Button)findViewById(R.id.mul)).setOnClickListener(this);
		((Button)findViewById(R.id.div)).setOnClickListener(this);
		
		((Button)findViewById(R.id.ac)).setOnClickListener(this);
		((Button)findViewById(R.id.bs)).setOnClickListener(this);
		
		((Button)findViewById(R.id.dot)).setOnClickListener(this);
		((Button)findViewById(R.id.equ)).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    String output = "0";
    double input1 = 0.0;
    int sign = 0;

    void computefunc(){
    double input2 = Double.parseDouble(output);
    double res = 0;
    if (sign == 1){
    res = input1 + input2;
    }
    else if (sign == 2){
    res = input1 - input2;
    }
    else if(sign == 3){
    if(input2 == 0){
    res = input1 * 1;
    }else{
    res = input1 * input2;
    }
    }
    else if(sign == 4 ){
    if(input2 == 0){
    res = input1/1;
    }else{
    res = input1 / input2;
    }
    }
    output = res + "";
    }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		TextView tvOutput = (TextView)findViewById(R.id.output);
		TextView op = (TextView)findViewById(R.id.operator);
		
		switch(id){
		case R.id.num0:
		case R.id.num1:
		case R.id.num2:
		case R.id.num3:
		case R.id.num4:
		case R.id.num5:
		case R.id.num6:
		case R.id.num7:
		case R.id.num8:
		case R.id.num9:
			output += ((Button)v).getText().toString();

			if (output.charAt(0) == '0')
			output = output.substring(1);

			if (output.length() <= 0)
			output = "0";

			tvOutput.setText(output);
			break;
		
		
		case R.id.ac:
			input1 = 0;
			sign = 0;
			output = "0";
			op.setText(""); 
			tvOutput.setText(output);		
			break;
			
		case R.id.bs:
			output = output.substring(0, output.length()-1);
			if (output.length() <= 0)
			{
				output = "0";
			}
			tvOutput.setText(output);		
			break;
			
		case R.id.add:
			if (sign == 0){
			sign = 1;
			input1 = Double.parseDouble(output);
			output = "0";
			// outputTV.setText(output);
			op.setText("+"); 
			}
			else
			{
			///////////
			computefunc();
			tvOutput.setText(output);
			/////////////

			sign = 1;
			input1 = Double.parseDouble(output);
			output = "0";
			// outputTV.setText(output);
			op.setText("+"); 
			tvOutput.setText(output);
			}
			break;
			
		case R.id.sub:
			if (sign == 0){
			sign = 2;
			input1 = Double.parseDouble(output);
			output = "0";
			// outputTV.setText(output);
			op.setText("-");
			}
			else
			{
			////////////
			computefunc();
			tvOutput.setText(output);
			/////////

			sign = 2;
			input1 = Double.parseDouble(output);
			output = "0";
			// outputTV.setText(output);
			op.setText("-");
			tvOutput.setText(output);
			}

			break;

			case R.id.mul:
			if (sign == 0){
			sign = 3;
			input1 = Double.parseDouble(output);
			output = "0";
			// outputTV.setText(output);
			op.setText("x");
			}
			else
			{
			////////////
			computefunc();
			tvOutput.setText(output);
			/////////

			sign = 3;
			input1 = Double.parseDouble(output);
			output = "0";
			// outputTV.setText(output);
			op.setText("x");
			tvOutput.setText(output);
			}

			break;
			
		case R.id.div:
			if (sign == 0){
			sign = 4;
			input1 = Double.parseDouble(output);
			output = "0";
			// outputTV.setText(output);
			op.setText("�");
			}
			else
			{
			////////////
			computefunc();
			tvOutput.setText(output);
			/////////

			sign = 4;
			input1 = Double.parseDouble(output);
			output = "0";
			// outputTV.setText(output);
			op.setText("�");
			tvOutput.setText(output);
			}

			break;
			
		case R.id.equ:
			computefunc();
			tvOutput.setText(output);
			op.setText("");
			sign=0;
			break; 
			
		case R.id.dot:
			output += ".";
			tvOutput.setText(output);
			break;
		}
		
		
	}
    
}
